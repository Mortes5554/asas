package com.icerock.labs_java;

import org.junit.Assert;
import org.junit.Test;

import static com.icerock.labs_java.util.LabUtil.buildList;
import static com.icerock.labs_java.util.LabUtil.print;

public class AppTest {

    @Test
    public void lab1Test() {
        print.accept("Lab 1 - test start");
        Lab1 lab1 = new Lab1(buildList(1D, 1D), "+", 3);
        Assert.assertEquals(lab1.result, 2, 0);
        lab1 = new Lab1(buildList(5D, 1D), "*", 3);
        Assert.assertEquals(lab1.result, 5, 0);
        lab1 = new Lab1(buildList(10D, 2D), "/", 3);
        Assert.assertEquals(lab1.result, 5, 0);
        lab1 = new Lab1(buildList(10D, 2D), "-", 3);
        Assert.assertEquals(lab1.result, 8, 0);
        print.accept("Lab 1 - test completed");
    }

    @Test
    public void lab2Test() {
        print.accept("Lab 2 - test start");
        Lab2 lab2 = new Lab2(36D);
        Assert.assertEquals(lab2.result, 6, 0);
        lab2 = new Lab2(144D);
        Assert.assertEquals(lab2.result, 12, 0);
        lab2 = new Lab2(900D);
        Assert.assertEquals(lab2.result, 30, 0);
        lab2 = new Lab2(8100D);
        Assert.assertEquals(lab2.result, 90, 0);
        print.accept("Lab 2 - test completed");
    }

    @Test
    public void lab3Test() {
        print.accept("Lab 3 - test start");
        Lab3 lab3 = new Lab3(1, "--poly=1,3,4,s,f,2");
        Assert.assertEquals(lab3.result, 2.083333333333333, 0);
        lab3 = new Lab3(2, "--otherParam=121", "--poly=1,3,4,s,f,2");
        Assert.assertEquals(lab3.result, 4.166666666666666, 0);
        lab3 = new Lab3(3, "3", "4", "--poly=1,3,4,s,f,2");
        Assert.assertEquals(lab3.result, 6.25, 0);
        lab3 = new Lab3(4, "--poly=1,3,4,s,f,2", "--otherParam=12");
        Assert.assertEquals(lab3.result, 8.333333333333332, 0);
        print.accept("Lab 3 - test completed");
    }

    @Test
    public void lab4Test() {
        print.accept("Lab 4 - test start");
        Lab4 lab4 = new Lab4(5, 25);
        print.accept("Lab 4 - test completed");
    }

    @Test
    public void lab5Test() {
        print.accept("Lab 5 - test start");
        Lab5 lab5 = new Lab5();
        print.accept("Lab 5 - test completed");
    }
}
