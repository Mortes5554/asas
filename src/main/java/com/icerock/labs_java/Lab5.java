package com.icerock.labs_java;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.io.FileUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.statistics.HistogramDataset;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.icerock.labs_java.util.LabUtil.print;
import static java.lang.String.format;
import static java.util.Arrays.stream;

class Lab5 {

    Lab5() {
        MailProcessor processor = new MailProcessor();
        List<MailDto> mails = processor.getMails();
        print.accept(format("X-DSPAM-Confidence avg = %s", processor.mean(mails, MailDto::getConfidence)));

        Map<String, Long> mailByAuthor = mails.stream().collect(Collectors.groupingBy(MailDto::getFrom, Collectors.counting()));
        processor.saveHistogram(mailByAuthor);
        print.accept("Histogram Saved!");

        print.accept("Spammers:");
        Map<String, Long> spammers = processor.getSpammers(mailByAuthor);
        spammers.forEach((name, mail_count) -> print.accept(name + ": " + mail_count));
    }

    @Data
    private static class MailProcessor {
        private List<MailDto> mails;

        MailProcessor() {
            mails = processMail(getData());
        }

        BigDecimal mean(List<MailDto> source, Function<MailDto, BigDecimal> meanField) {
            return source.stream().map(meanField).reduce(BigDecimal::add)
                    .map(i -> i.divide(BigDecimal.valueOf(source.size()), MathContext.DECIMAL128))
                    .orElse(BigDecimal.ZERO);
        }

        private String getData() {
            try {
                File file = FileUtils.toFile(getClass().getResource("/mbox.txt"));
                return FileUtils.readFileToString(file, Charset.forName("utf-8"));
            } catch (IOException ex) {
                print.accept(ex.getMessage());
                return null;
            }
        }

        private List<MailDto> processMail(String data) {
            List<MailDto> result = new ArrayList<>();

            if (data != null) {
                stream(data.split("----------------------")).forEach(item -> {
                    Map<String, String> params = stream(item.split("\n"))
                            .map(i -> i.split(":"))
                            .filter(i -> i.length == 2)
                            .collect(Collectors.toMap(i -> i[0], j -> j[1], (x, y) -> x));
                    if (params.containsKey("From")) {
                        result.add(MailDto.builder()
                                .from(params.get("From"))
                                .confidence(toNumber(params.get("X-DSPAM-Confidence")))
                                .probability(toNumber(params.get("X-DSPAM-Probability")))
                                .build());
                    }
                });
            }

            return result;
        }

        void saveHistogram(Map<String, Long> source) {
            try {

                HistogramDataset dataset = new HistogramDataset();
                source.forEach((label, value) -> dataset.addSeries(label, new double[]{value.doubleValue()}, 10));
                JFreeChart chart = ChartFactory.createHistogram("Histogram", "Author", "Main Count",
                        dataset, PlotOrientation.VERTICAL, false, false, false);

                ChartUtils.saveChartAsPNG(new File("histogram.PNG"), chart, 500, 500);
            } catch (IOException e) {
                print.accept(e.getMessage());
            }
        }

        private Map<String, Long> getSpammers(Map<String, Long> mailByAuthor) {
            return mailByAuthor.entrySet().stream()
                    .sorted((o1, o2) -> Long.compare(o2.getValue(), o1.getValue()))
                    .limit(5).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }

        private BigDecimal toNumber(String value) {
            try {
                return BigDecimal.valueOf(Double.valueOf(value));
            } catch (NumberFormatException ex) {
                return BigDecimal.ZERO;
            }
        }
    }

    @Data
    @Builder
    public static class MailDto {
        private String from;
        private BigDecimal probability;
        private BigDecimal confidence;
    }
}
