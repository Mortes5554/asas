package com.icerock.labs_java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    private static final Logger LOG = LoggerFactory.getLogger(App.class);
    private static final String LAB_TASK_TEMPLATE = "Выполнение лабораторного задание № {}";

    public static void main(String[] args) {
        LOG.info(LAB_TASK_TEMPLATE, 1);
        Lab1 lab1 = new Lab1();
        LOG.info(LAB_TASK_TEMPLATE, 2);
        Lab2 lab2 = new Lab2();
        LOG.info(LAB_TASK_TEMPLATE, 3);
        Lab3 lab3 = new Lab3();
        LOG.info(LAB_TASK_TEMPLATE, 4);
        Lab4 lab4 = new Lab4();
        LOG.info(LAB_TASK_TEMPLATE, 5);
        Lab5 lab5 = new Lab5();
    }
}
