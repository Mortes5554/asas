package com.icerock.labs_java;

import lombok.Getter;
import lombok.Setter;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

import static com.icerock.labs_java.util.LabUtil.print;

class Lab1 {
    private static Scanner INPUT = new Scanner(System.in);
    private static List<String> OPERATIONS = List.of("+", "-", "*", "/");
    private static ScriptEngine EXECUTOR = new ScriptEngineManager().getEngineByName("js");
    double result;

    Lab1(List<Double> operands, String operation, int action) {
        Calculator calculator = new Calculator();
        calculator.getMultipliers().addAll(operands);
        calculator.setOperation(operation);

        result = calculator.calc();
        while (true) {
            switch (action) {
                case 1:
                    print.accept("Результат предыдущей операции был сохранен в переменную x.");
                    calculator.clean();
                    calculator.getMultipliers().add(result);
                    calculator.setOperation(operation);
                    calculator.getMultipliers().add(operands.get(1));
                    calculator.calc();
                    break;
                case 2:
                    calculator.clean();
                    calculator.setMultipliers(operands);
                    calculator.calc();
                    break;
                default:
                    return;
            }
        }
    }

    Lab1() {
        Calculator calculator = new Calculator();
        calculator.inputData();
        result = calculator.calc();
        int action;

        while (true) {
            action = inputInt.apply("Выберите действие:\n 1) Продолжение работы над операндом\n 2) Ввод новых данных\n 3) Выход\n");

            switch (action) {
                case 1:
                    print.accept("Результат предыдущей операции был сохранен в переменную x.");
                    calculator.clean();
                    calculator.getMultipliers().add(result);
                    calculator.inputYAndOperation();
                    calculator.calc();
                    break;
                case 2:
                    calculator.clean();
                    calculator.inputData();
                    calculator.calc();
                    break;
                default:
                    return;
            }
        }
    }

    @Getter
    @Setter
    private static class Calculator {
        private String operation;
        private List<Double> multipliers = new ArrayList<>();

        void inputData() {
            multipliers.add(inputDouble.apply("Введите Х:"));
            multipliers.add(inputDouble.apply("Введите Y:"));
            operation = inputOperation.apply("Выберите операцию");
        }

        void inputYAndOperation() {
            multipliers.add(inputDouble.apply("Введите Y:"));
            operation = inputOperation.apply("Выберите операцию");
        }

        double calc() {
            try {
                Object result = EXECUTOR.eval(String.format("%s %s %s", multipliers.get(0), operation, multipliers.get(1)));
                print.accept("Ответ: " + result);
                return (double) result;
            } catch (ScriptException e) {
                print.accept(e.getMessage());
                return 0;
            }
        }

        void clean() {
            multipliers.clear();
        }
    }


    private static Function<String, Integer> inputInt = msg -> {
        print.accept(msg);
        return INPUT.nextInt();
    };
    private static Function<String, Double> inputDouble = msg -> {
        print.accept(msg);
        return INPUT.nextDouble();
    };
    private static Function<String, String> inputOperation = msg -> {
        print.accept(msg);
        String operation = INPUT.next();
        while (!OPERATIONS.contains(operation)) {
            print.accept("Введено неверное значение, попробуйте еще раз!");
            operation = INPUT.next();
        }
        return operation;
    };
}
