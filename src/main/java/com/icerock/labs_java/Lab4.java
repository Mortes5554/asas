package com.icerock.labs_java;

import lombok.Data;
import org.apache.commons.lang3.tuple.Pair;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.icerock.labs_java.util.LabUtil.print;

class Lab4 {
    Lab4() {
        Sma sma = new Sma();
        sma.setWindowSize(5);
        sma.start(25);
    }

    Lab4(int windowSize, int iteration) {
        Sma sma = new Sma();
        sma.setWindowSize(windowSize);
        sma.start(iteration);
    }

    @Data
    private static class Sma {
        private LinkedList<Pair<Long, Double>> queue = new LinkedList<>();
        private LinkedList<Double> smaQueue = new LinkedList<>();
        private int windowSize;

        void start(int n) {
            Random random = new Random();
            IntStream.range(0, n).forEach(i -> {
                push(random.nextInt());
                sleep(1000);

                if (queue.size() == windowSize) {
                    calc_sma();
                    saveChart();
                    print.accept("Chart saved!");
                }
            });
        }

        private void saveChart() {
            try {
                DefaultCategoryDataset dataset = new DefaultCategoryDataset();
                queue.stream().map(Pair::getValue)
                        .forEach(i -> dataset.addValue(i, "data", "data"));
                JFreeChart chart = ChartFactory.createLineChart("Sma", null, null,
                        dataset, PlotOrientation.VERTICAL, false, false, false);

                ChartUtils.saveChartAsPNG(new File("SMA.PNG"), chart, 500, 500);
            } catch (IOException ex) {
                print.accept(ex.getMessage());
            }
        }

        private void push(Integer item) {
            queue.addFirst(Pair.of(System.currentTimeMillis(), Double.valueOf(item)));
            if (queue.size() > windowSize) {
                queue.pollLast();
            }
            print.accept("Вы ввели: " + item);
        }

        private void calc_sma() {
            double mean = mean();
            smaQueue = queue.stream().map(i -> (i.getValue() - mean) / queue.size())
                    .collect(Collectors.toCollection(LinkedList::new));

        }

        private double mean() {
            return queue.stream().map(Pair::getValue).reduce(0D, Double::sum) / queue.size();
        }
    }

    private static void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            print.accept(ex.getMessage());
        }
    }
}
