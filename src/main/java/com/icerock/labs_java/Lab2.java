package com.icerock.labs_java;

import java.util.Scanner;

import static com.icerock.labs_java.util.LabUtil.print;

class Lab2 {
    private static final Scanner INPUT = new Scanner(System.in);
    double result;

    Lab2() {
        Sqrt sqrt = new Sqrt();
        result = sqrt.calc();
        print.accept("Результат " + result + "\n");
    }

    Lab2(Double num) {
        Sqrt sqrt = new Sqrt(num);
        result = sqrt.calc();
        print.accept("Результат " + result + "\n");
    }


    private static class Sqrt {
        private Double num;

        Sqrt() {
            print.accept("Введите число: ");
            this.num = INPUT.nextDouble();
        }

        Sqrt(Double num) {
            this.num = num;
        }

        private Double calc() {
            Double prev = null;
            Double x = 1d;

            while (true) {
                x = 0.5 * (x + num / x);
                if (prev != null && prev.equals(x)) {
                    return x;
                }
                prev = x;
            }
        }
    }
}
